# Ansible Prometheus Alertmanager role.

Роль устанавливает последнюю версию Prometheus Alertmanager с GitHub.
  
  ## По умолчанию используются следующие переменные:
alertmanager_user: alertmanager
alertmanager_group: alertmanager
alertmanager_tmp_dir: /tmp
alertmanager_bin: /usr/local/bin/alertmanager
alertmanager_conf_dir: /etc/alertmanager
alertmanager_data_dir: /var/lib/alertmanager
alertmanager_bind_addr: 0.0.0.0
alertmanager_bind_port: 9093

